#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include "../inc/lcd_sysfs.h"
#include "../inc/lcd_cdev.h"
#include "../inc/lcd_display.h"

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Maksym Bilyk <makkusu.shiroi@gmail.com>");
MODULE_DESCRIPTION("LCD Driver");

int init_module(void)
{
	int ret;
	if ((ret = dev_init())) {
		pr_info("Failed to initialize charcter device\n");
		goto out;
	}
	if ((ret = sysfs_init())) {
		pr_info("Failed to initialize sysfs\n");
		goto out_cdev;
	}
	if ((ret = display_init())) {
		pr_info("Failed to initialize display\n");
		goto out_sysfs;
	}
	return 0;

out_sysfs:
	sysfs_exit();
out_cdev:
	dev_exit();
out:
	return ret;
}

void cleanup_module(void)
{
	sysfs_exit();
	dev_exit();
	display_exit();
	pr_info("LCD: spi device unregistered\n");
	pr_info("LCD: module exited\n");
}
