#include <linux/kernel.h>
#include <linux/device/class.h>
#include "../inc/user_api.h"
#include "../inc/lcd_display.h"

static ssize_t command_store(struct class *class, struct class_attribute *attr,
			     const char *buf, size_t count)
{
	int command = -1;
	pr_info("command:: %s\n", buf);
	sscanf(buf, "%d", &command);
	switch (command) {
	case DRAW_RECTANGLE:
		lcd_command = DRAW_RECTANGLE;
		break;
	case DRAW_BALL:
		lcd_command = DRAW_BALL;
		break;
	case RENDER:
		lcd_command = RENDER;
		break;
	case SET_WINDOW:
		lcd_command = SET_WINDOW;
		break;
	default:
		pr_info("command error: unknown command %d\n", command);
	}
	return 0;
}

CLASS_ATTR_WO(command);

struct class *display_class;

int sysfs_init(void)
{
	int err;
	err = -1;
	display_class = class_create(THIS_MODULE, "display");
	if (IS_ERR(display_class)) {
		pr_info("Failed to create display class\n");
		goto end;
	}
	if ((err = class_create_file(display_class, &class_attr_command))) {
		pr_info("Failed to create command-file\n");
		goto end;
	}
end:
	return err;
}

void sysfs_exit(void)
{
	class_remove_file(display_class, &class_attr_command);
	class_destroy(display_class);
}
