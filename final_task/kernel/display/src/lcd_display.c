#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include <linux/device/class.h>
#include "../inc/lcd_cdev.h"
#include "../inc/ili9341.h"
#include "../inc/fonts.h"
#include "../inc/lcd_display.h"

#define DATA_SIZE 90
#define PIXEL_SIZE sizeof(pixel_t)

static bool is_full_screen(struct window *w);
static void lcd_write_command(u8 cmd);
static void swap_full_screen(void);
static void lcd_write_data(u8 *buff, size_t buff_size);
static bool is_full_screen(struct window *w);
inline static void lcd_reset(void);

static struct spi_device *lcd_spi_device;
static struct window curw = {
	.x0 = 0,
	.x1 = LCD_WIDTH - 1,
	.y0 = 0,
	.y1 = LCD_HEIGHT - 1,
	.w = LCD_WIDTH,
	.h = LCD_HEIGHT,
};

static struct frame fbuff = {
	.size = LCD_HEIGHT * LCD_WIDTH,
	.n_rows = LCD_HEIGHT,
	.n_cols = LCD_WIDTH,
};

static bool is_full_screen(struct window *w)
{
	return w->x0 == 0 && w->y0 == 0 && w->w == LCD_WIDTH &&
	       w->h == LCD_HEIGHT;
}

inline static void lcd_reset(void)
{
	gpio_set_value(LCD_PIN_RESET, 0);
	mdelay(5);
	gpio_set_value(LCD_PIN_RESET, 1);
}

static void lcd_write_command(u8 cmd)
{
	gpio_set_value(LCD_PIN_DC, 0);
	spi_write(lcd_spi_device, &cmd, sizeof(cmd));
}

static void lcd_write_data(u8 *buff, size_t buff_size)
{
	size_t i = 0;
	gpio_set_value(LCD_PIN_DC, 1);
	while (buff_size > DATA_SIZE) {
		spi_write(lcd_spi_device, buff + i, DATA_SIZE);
		i += DATA_SIZE;
		buff_size -= DATA_SIZE;
	}
	spi_write(lcd_spi_device, buff + i, buff_size);
}

void lcd_set_address_window(u16 x0, u16 y0, u16 w, u16 h)
{
	curw.x0 = x0;
	curw.x1 = x0 + w - 1;
	curw.y0 = y0;
	curw.y1 = y0 + h - 1;
	curw.w = w;
	curw.h = h;

	lcd_write_command(LCD_CASET);
	{
		uint8_t data[] = { (curw.x0 >> 8) & 0xFF, curw.x0 & 0xFF,
				   (curw.x1 >> 8) & 0xFF, curw.x1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RASET);
	{
		uint8_t data[] = { (curw.y0 >> 8) & 0xFF, curw.y0 & 0xFF,
				   (curw.y1 >> 8) & 0xFF, curw.y1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RAMWR);
}

void swap_full_screen(void)
{
	static struct window swapw = { .x0 = 0,
				       .x1 = LCD_WIDTH - 1,
				       .y0 = 0,
				       .y1 = LCD_HEIGHT - 1,
				       .w = LCD_WIDTH,
				       .h = LCD_HEIGHT };
	struct window tempw;

	if (is_full_screen(&curw) && is_full_screen(&swapw))
		return;

	tempw = curw;
	curw = swapw;
	swapw = tempw;
	lcd_set_address_window(curw.x0, curw.y0, curw.w, curw.h);
}

void lcd_full_update(void)
{
	swap_full_screen();
	lcd_write_data((u8 *)fbuff.display, fbuff.size * PIXEL_SIZE);
	swap_full_screen();
}

void lcd_fast_update(void)
{
	size_t row;
	for (row = curw.y0; row <= curw.y1; ++row) {
		memcpy(fbuff.unraveled + (row - curw.y0) * curw.w,
		       fbuff.display + row * fbuff.n_cols + curw.x0,
		       curw.w * PIXEL_SIZE);
	}

	pr_info("w: %u, h: %u\n", curw.w, curw.h);
	lcd_write_data((u8 *)fbuff.unraveled, curw.w * curw.h * PIXEL_SIZE);
}

void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color)
{
	u16 i;
	u16 j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			fbuff.display[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] =
				(color >> 8) | (color << 8);
		}
	}
}

void lcd_fill_screen(u16 color)
{
	lcd_fill_rectangle(0, 0, LCD_WIDTH, LCD_HEIGHT, color);
}

void lcd_put_char(u16 x, u16 y, char ch, FontDef font, u16 color, u16 bgcolor)
{
	u32 i, b, j;
	for (i = 0; i < font.height; i++) {
		b = font.data[(ch - 32) * font.height + i];
		for (j = 0; j < font.width; j++) {
			if ((b << j) & 0x8000) {
				fbuff.display[(x + LCD_WIDTH * y) +
					      (j + LCD_WIDTH * i)] =
					(color >> 8) | (color << 8);
			} else {
				fbuff.display[(x + LCD_WIDTH * y) +
					      (j + LCD_WIDTH * i)] =
					(bgcolor >> 8) | (bgcolor << 8);
			}
		}
	}
}

void lcd_put_str(u16 x, u16 y, const char *str, FontDef font, u16 color,
		 u16 bgcolor)
{
	while (*str) {
		if (x + font.width >= LCD_WIDTH) {
			x = 0;
			y += font.height;
			if (y + font.height >= LCD_HEIGHT) {
				break;
			}

			if (*str == ' ') {
				// skip spaces in the beginning of the new line
				str++;
				continue;
			}
		}
		lcd_put_char(x, y, *str, font, color, bgcolor);
		x += font.width;
		str++;
	}
}

void lcd_init_ili9341(void)
{
	// SOFTWARE RESET
	lcd_write_command(0x01);
	mdelay(1000);

	// POWER CONTROL A
	lcd_write_command(0xCB);
	{
		u8 data[] = { 0x39, 0x2C, 0x00, 0x34, 0x02 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL B
	lcd_write_command(0xCF);
	{
		u8 data[] = { 0x00, 0xC1, 0x30 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL A
	lcd_write_command(0xE8);
	{
		u8 data[] = { 0x85, 0x00, 0x78 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL B
	lcd_write_command(0xEA);
	{
		u8 data[] = { 0x00, 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER ON SEQUENCE CONTROL
	lcd_write_command(0xED);
	{
		u8 data[] = { 0x64, 0x03, 0x12, 0x81 };
		lcd_write_data(data, sizeof(data));
	}

	// PUMP RATIO CONTROL
	lcd_write_command(0xF7);
	{
		u8 data[] = { 0x20 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,VRH[5:0]
	lcd_write_command(0xC0);
	{
		u8 data[] = { 0x23 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,SAP[2:0];BT[3:0]
	lcd_write_command(0xC1);
	{
		u8 data[] = { 0x10 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL
	lcd_write_command(0xC5);
	{
		u8 data[] = { 0x3E, 0x28 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL 2
	lcd_write_command(0xC7);
	{
		u8 data[] = { 0x86 };
		lcd_write_data(data, sizeof(data));
	}

	// PIXEL FORMAT
	lcd_write_command(0x3A);
	{
		u8 data[] = { 0x55 };
		lcd_write_data(data, sizeof(data));
	}

	// FRAME RATIO CONTROL, STANDARD RGB COLOR
	lcd_write_command(0xB1);
	{
		u8 data[] = { 0x00, 0x18 };
		lcd_write_data(data, sizeof(data));
	}

	// DISPLAY FUNCTION CONTROL
	lcd_write_command(0xB6);
	{
		u8 data[] = { 0x08, 0x82, 0x27 };
		lcd_write_data(data, sizeof(data));
	}

	// 3GAMMA FUNCTION DISABLE
	lcd_write_command(0xF2);
	{
		u8 data[] = { 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// GAMMA CURVE SELECTED
	lcd_write_command(0x26);
	{
		u8 data[] = { 0x01 };
		lcd_write_data(data, sizeof(data));
	}

	// POSITIVE GAMMA CORRECTION
	lcd_write_command(0xE0);
	{
		u8 data[] = { 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, 0x4E, 0xF1,
			      0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// NEGATIVE GAMMA CORRECTION
	lcd_write_command(0xE1);
	{
		u8 data[] = { 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, 0x31, 0xC1,
			      0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F };
		lcd_write_data(data, sizeof(data));
	}

	// EXIT SLEEP
	lcd_write_command(0x11);
	mdelay(120);

	// TURN ON DISPLAY
	lcd_write_command(0x29);

	// MEMORY ACCESS CONTROL
	lcd_write_command(0x36);
	{
		u8 data[] = { 0x28 };
		lcd_write_data(data, sizeof(data));
	}
}

int display_init(void)
{
	int ret;
	struct spi_master *master;
	struct spi_board_info lcd_info = {
		.modalias = "LCD",
		.max_speed_hz = 50e6,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	};

	master = spi_busnum_to_master(lcd_info.bus_num);
	if (!master) {
		printk("MASTER not found.\n");
		ret = -ENODEV;
		goto end;
	}

	lcd_spi_device = spi_new_device(master, &lcd_info);
	if (!lcd_spi_device) {
		printk("FAILED to create slave.\n");
		ret = -ENODEV;
		goto end;
	}

	lcd_spi_device->bits_per_word = 8,

	ret = spi_setup(lcd_spi_device);
	if (ret) {
		printk("FAILED to setup slave.\n");
		spi_unregister_device(lcd_spi_device);
		ret = -ENODEV;
		goto end;
	}

	pr_info("LCD: spi device setup completed\n");

	gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
	gpio_direction_output(LCD_PIN_RESET, 0);
	gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
	gpio_direction_output(LCD_PIN_DC, 0);
	lcd_reset();
	lcd_init_ili9341();
	mdelay(10);
	lcd_set_address_window(0, 0, LCD_WIDTH, LCD_HEIGHT);
	lcd_fill_screen(COLOR_MAGENTA);
	lcd_full_update();

end:
	return ret;
}

void display_exit(void)
{
	lcd_fill_screen(COLOR_WHITE);
	lcd_full_update();
	gpio_free(LCD_PIN_DC);
	if (lcd_spi_device) {
		spi_unregister_device(lcd_spi_device);
	}
}
