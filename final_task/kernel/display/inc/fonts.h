#ifndef __FONTS_H__
#define __FONTS_H__

typedef struct {
	const u8 width;
	uint8_t height;
	const uint16_t *data;
} FontDef;

extern FontDef Font_7x10;
extern FontDef Font_11x18;
extern FontDef Font_16x26;
extern FontDef Ball_16x16;

#endif // __FONTS_H__
