#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "../kernel/display/inc/user_api.h"
#include "config.h"
#include <time.h>
#include <errno.h>

int msleep(long msec)
{
	struct timespec ts;
	int res;

	if (msec < 0) {
		errno = EINVAL;
		return -1;
	}

	ts.tv_sec = msec / 1000;
	ts.tv_nsec = (msec % 1000) * 1000000;

	do {
		res = nanosleep(&ts, &ts);
	} while (res && errno == EINTR);

	return res;
}

char buf[100] = { '\0' };
int data_fd = -1;
int command_fd = -1;
int keys_fd = -1;
int is_end = 0;

void draw_ball(unsigned x, unsigned y, unsigned color)
{
	sprintf(buf, "%u", DRAW_BALL);
	write(command_fd, buf, strlen(buf));
	sprintf(buf, "%u|%u|%u", y, x, color);
	write(data_fd, buf, strlen(buf));
}
void draw_rectangle(unsigned x, unsigned y, unsigned w, unsigned h,
		    unsigned color)
{
	sprintf(buf, "%u", DRAW_RECTANGLE);
	write(command_fd, buf, strlen(buf));
	sprintf(buf, "%u|%u|%u|%u|%u", y, x, h, w, color);
	write(data_fd, buf, strlen(buf));
}

void update_all()
{
	sprintf(buf, "%u", RENDER);
	write(command_fd, buf, strlen(buf));
	sprintf(buf, "%u", 0);
	write(data_fd, buf, strlen(buf));
	msleep(100);
}

void update_window()
{
	sprintf(buf, "%u", RENDER);
	write(command_fd, buf, strlen(buf));
	sprintf(buf, "%u", 1);
	write(data_fd, buf, strlen(buf));
}

void set_window(unsigned x, unsigned y, unsigned w, unsigned h)
{
	sprintf(buf, "%u", SET_WINDOW);
	write(command_fd, buf, strlen(buf));
	sprintf(buf, "%u|%u|%u|%u", y, x, h, w);
	write(data_fd, buf, strlen(buf));
}

unsigned get_keys()
{
	unsigned keys = 0;
	read(keys_fd, buf, 1);
	lseek(keys_fd, 0, SEEK_SET);
	buf[1] = '\0';
	sscanf(buf, "%u", &keys);
	return (keys <= 7) ? keys : 0;
}

void draw_brick(struct figure *tb)
{
	set_window(tb->x0, tb->y0, tb->w, tb->h);
	draw_rectangle(tb->x0, tb->y0, tb->w, tb->h, tb->color);
	update_window();
}

void move_table(struct figure *tb, struct figure *cnt, int vector)
{
	int next_x0 = tb->x0 + vector * tb->incx;
	if (next_x0 < 0 || next_x0 + tb->w > cnt->w)
		return;
	set_window(tb->x0, tb->y0, tb->w, tb->h);
	draw_rectangle(tb->x0, tb->y0, tb->w, tb->h, cnt->color);
	update_window();
	tb->x0 = next_x0;
	set_window(tb->x0, tb->y0, tb->w, tb->h);
	draw_rectangle(tb->x0, tb->y0, tb->w, tb->h, tb->color);
	update_window();
}

int face_obstackle(struct figure *obs, struct figure *bl, int x, int y)
{
	int obstacles = 0;
	struct figure *cur = obs;
	int x00, x01, y00, y01, x10, x11, y10, y11;
	x00 = x;
	x01 = x00 + bl->w - 1;
	y00 = y;
	y01 = y00 + bl->h - 1;
	do {
		if (cur->state != DEAD) {
			++obstacles;
			x10 = cur->x0;
			x11 = x10 + cur->w - 1;
			y10 = cur->y0;
			y11 = y10 + cur->h - 1;
			if ((x00 <= x11) && (x01 >= x10) &&
			    (((y01 >= y10) && (y00 <= y10)) ||
			     ((y00 <= y11) && (y01 > y11)))) {
				bl->incy *= -1;
				if ((x00 >= x11) || (x01 <= x10))
					bl->incx *= -1;
				if (cur->state == MUTABLE) {
					cur->state = DEAD;
					cur->color = COLOR_BLACK;
					draw_brick(cur);
				}
				return 1;
			}
		}
		cur = cur->next;
	} while (cur != obs);
	if (obstacles == 1)
		is_end = 1;
	return 0;
}

void move_ball(struct figure *bl, struct figure *cnt, struct figure *obs)
{
	int next_y0 = bl->y0 + bl->incy;
	int next_x0 = bl->x0 + bl->incx;
	if (next_y0 < 0) {
		bl->incy *= -1;
		is_end = 1;
		return;
	}

	if (next_y0 + bl->h > cnt->h) {
		bl->incy *= -1;
		return;
	}
	if (next_x0 < 0) {
		bl->incx *= -1;
		return;
	}
	if (next_x0 + bl->w > cnt->w) {
		bl->incx *= -1;
		return;
	}

	if (face_obstackle(obs, bl, next_x0, next_y0)) {
		return;
	}
	set_window(bl->x0, bl->y0, bl->w, bl->h);
	draw_rectangle(bl->x0, bl->y0, bl->w, bl->h, cnt->color);
	update_window();
	bl->y0 = next_y0;
	bl->x0 = next_x0;
	set_window(bl->x0, bl->y0, bl->w, bl->h);
	draw_ball(bl->x0 + 1, bl->y0 + 1, bl->color);
	update_window();
}

int main(void)
{
	struct figure *cur;
	struct figure display = {
		.color = COLOR_BLACK,
		.h = LCD_WIDTH,
		.w = LCD_HEIGHT,
		.x0 = 0,
		.y0 = 0,
		.incx = 0,
		.incy = 0,
	};

	struct figure table = {
		.state = IMMUTABLE,
		.color = COLOR_RED,
		.h = 10,
		.w = 60,
		.x0 = 100,
		.y0 = 10,
		.incx = 20,
		.incy = 0,
	};

	struct figure ball = {
		.color = COLOR_BLUE,
		.h = 18,
		.w = 18,
		.x0 = 100,
		.y0 = 200,
		.incx = -1,
		.incy = -1,
	};
	unsigned keys = 0;
	data_fd = open("/dev/display", O_WRONLY);
	command_fd = open("/sys/class/display/command", O_WRONLY);
	keys_fd = open("/sys/class/lcd_keys/keys", O_RDONLY);
	if (data_fd < 0 || command_fd < 0 || keys_fd < 0) {
		printf("%d, %d, %d\n", data_fd, command_fd, keys_fd);
		return -1;
	}

	draw_rectangle(display.x0, display.y0, display.w, display.h,
		       display.color);
	update_all();

	brick_Head.next = &table;
	table.next = &brick_Tail;

	cur = &table;
	do {
		draw_brick(cur);
		cur = cur->next;
	} while (cur != &table);

	draw_brick(&table);
	get_keys();
	while (!((keys = get_keys()) & 0x2) && !is_end) {
		move_ball(&ball, &display, &table);
		msleep(10);
		if (keys & 0x4) {
			move_table(&table, &display, -1);
		} else if (keys & 0x1) {
			move_table(&table, &display, 1);
		}
	}
	draw_rectangle(display.x0, display.y0, display.w, display.h,
		       display.color);
	update_all();
	return 0;
}
