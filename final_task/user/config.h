#include "../kernel/display/inc/user_api.h"

enum states { IMMUTABLE, MUTABLE, DEAD };

struct figure {
	unsigned color;
	enum states state;
	int h;
	int w;
	int x0;
	int y0;
	int incx;
	int incy;
	struct figure *next;
};

struct figure brick_Head = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 0,
	.y0 = 300,
	.incx = 0,
	.incy = 0,
};

struct figure brick1 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 60,
	.y0 = 300,
	.incx = 0,
	.incy = 0,
	.next = &brick_Head,
};

struct figure brick2 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 120,
	.y0 = 300,
	.incx = 0,
	.incy = 0,
	.next = &brick1,
};

struct figure brick3 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 180,
	.y0 = 300,
	.incx = 0,
	.incy = 0,
	.next = &brick2,
};

struct figure brick4 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 0,
	.y0 = 280,
	.incx = 0,
	.incy = 0,
	.next = &brick3,
};

struct figure brick5 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 60,
	.y0 = 280,
	.incx = 0,
	.incy = 0,
	.next = &brick4,
};

struct figure brick6 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 120,
	.y0 = 280,
	.incx = 0,
	.incy = 0,
	.next = &brick5,
};

struct figure brick7 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 180,
	.y0 = 280,
	.incx = 0,
	.incy = 0,
	.next = &brick6,
};

struct figure brick8 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 0,
	.y0 = 260,
	.incx = 0,
	.incy = 0,
	.next = &brick7,
};

struct figure brick9 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 60,
	.y0 = 260,
	.incx = 0,
	.incy = 0,
	.next = &brick8,
};

struct figure brick10 = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 120,
	.y0 = 260,
	.incx = 0,
	.incy = 0,
	.next = &brick9,
};

struct figure brick_Tail = {
	.state = MUTABLE,
	.color = COLOR_RED,
	.h = 10,
	.w = 50,
	.x0 = 180,
	.y0 = 260,
	.incx = 0,
	.incy = 0,
	.next = &brick10,
};
