#include <linux/string.h>
#include <linux/device/class.h>
#include <linux/jiffies.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Maksym Bilyk");
MODULE_DESCRIPTION("Example for procfs read/write");
MODULE_VERSION("0.1");

#define MODULE_TAG     "task_07"
#define BUFFER_SIZE    100

static u32 last_read;
static u32 cur_read;
char read_mod[BUFFER_SIZE];

static ssize_t time_point_show(struct class *class, struct class_attribute *attr,
		char *buf)
{
	cur_read = jiffies; 
	sprintf(buf, ": %u", (cur_read-last_read)/HZ);
	last_read=cur_read;
	return strlen(buf);
}

static ssize_t time_point_store( struct class *class, struct class_attribute *attr,
		const char *buf, size_t count )
{
	return count;
}

CLASS_ATTR_RW( time_point );
static struct class *time_point_class;

static int __init example_init(void)
{
	int err;
	time_point_class = class_create(THIS_MODULE, MODULE_TAG);
	if (IS_ERR(time_point_class)) printk("bad class create\n");
	err = class_create_file(time_point_class, &class_attr_time_point);
	printk(KERN_NOTICE MODULE_TAG "loaded\n");
	last_read = jiffies;
    	return 0;
}

static void __exit example_exit(void)
{
    class_remove_file(time_point_class, &class_attr_time_point);
    class_destroy(time_point_class);
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(example_init);
module_exit(example_exit);
