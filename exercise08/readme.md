# GuideLine
- create bash-script for exporting ENV variables

 `init`                                                                                              
```bash
#! /bin/bash

declare build_kernel=k5.7.b
declare build_rootfs=rfs5.7.b
declare src_kernel=k5.7.s
declare src_rootfs=rfs.s

mkdir -p ${build_kernel} ${build_rootfs} ${src_kernel} ${src_rootfs}

export BUILD_KERNEL=$(pwd)/${build_kernel}
export BUILD_ROOTFS=$(pwd)/${build_rootfs}

export SOURCE_KERNEL=$(pwd)/${src_kernel}
export SOURCE_ROOTFS=$(pwd)/${src_rootfs}
```
- run script
	- `source ./init`
-  fetch KERNEL + BUILDROOT sources
	- `git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable -b v5.7 --depth 1 ${SOURCE_KERNEL}`
	- `git clone git://git.buildroot.net/buildroot -b 2021.05.x ${SOURCE_ROOTFS}`
- prepare build directories for KERNEL and ROOTFS                                                                         
	- `make O=${BUILD_KERNEL} -C ${SOURCE_KERNEL} i386_defconfig`
	- `make O=${BUILD_ROOTFS} -C ${SOURCE_ROOTFS} qemu_x86_defconfig`
- launch `menuconfig` target to configure KERNEL
	- `make -C ${BUILD_KERNEL} menuconfig`
- build KERNEL
	- `make -C ${BUILD_KERNEL} -j4`
- launch `menuconfig` target to configure ROOTFS
	- `make -C ${BUILD_ROOTFS} menuconfig`
---
### Target options --->
- Target Architecture **(i386)**
- Target Architecture Variant **(i686)**
### Toolchain --->
- Custom kernel headers series **(5.7.x)**
-  **[\*]** Enable WCHAR support
### System configuration --->
- **(Linux_v5.7)** System hostname  
- **(Welcome to Linux_v5.7)** System banner  
- **[\*]** Enable root login with password  
- **(rootpass)** Root password 
- **(${BUILD_ROOTFS}/users)** Path to the users tables
- **(${BUILD_ROOTFS}/root)** Root filesystem overlay directories
### Kernel --->
- **[ ]** Linux Kernel
### Target packages --->
- **[\*]** Show packages that are also provided by busybox  
- **Development tools --->**  
	- **[\*]** binutils  
	- **[\*]** binutils binaries  
	- **[\*]** findutils  
	- **[\*]** grep  
	- **[\*]** sed  
	- **[\*]** tree  
- **Libraries ---> Compression and decompression --->**
	- **[\*]** zlib support
- **Libraries ---> Text and terminal handling --->** 
	- **[\*]** ncurses
	- **[\*]** readline
- **Networking applications --->**
	- **[\*]** dropbear
	- **[\*]** wget
- **Shell and utilities --->**
	- **[\*]** bash  
	- **[\*]** file  
	- **[\*]** sudo  
	- **[\*]** which
- **System tools --->** 
	- **[\*]** kmod  
	- **[\*]** kmod utilities  
	- **[\*]** rsyslog  
- **Text editors and viewers --->**  
	- **[\*]** joe  
	- **[\*]** less  
	- **[\*]** mc  
	- **[\*]** vim

### Filesystem images --->
- **[\*]** ext2/3/4 root filesystem
- ext2/3/4 variant **(ext3)**
- **[\*]** tar the root filesystem
---
- additional files
```bash
# Create user record
echo "user 1000 user 1000 =userpass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users
# Add user user to sodoers
mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d  
echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user
# Create list of shells for dropbear
mkdir -p ${BUILD_ROOTFS}/root/etc  
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells  
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells
```
- build ROOTFS
	- `make -C ${BUILD_ROOTFS} -j4`
- install QEMU (in Fedora)
	- `sudo dnf install qemu`
- Launch QEMU
 ```bash
qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -nic user,hostfwd=tcp::8022-:22 &
 ```
 - generate ssh key
	 - `ssh-keygen -t rsa`
- copy key to the QEMU
	- `scp -P 8022 <full-path-to-the-key> user@localhost:`
- enter QEMU using ssh and add key to authorized_keys file
	- `ssh -p 8022 user@localhost`
	- `mkdir .ssh`
	- `cat id_rsa_5_7.pub > .ssh/authorized_keys`
	- `exit`
- update `~/.ssh/config` file
	- `echo -e 'HOST 5_7\n\tHostName localhost\n\tPort 8022\n\tUser user\n\tIdentityFile <full-path-to-the-key>' >>  ~/.ssh/config`
- enter QEMU using ssh
	- `ssh 5_7`
- get system info by running following script
```bash
#! /bin/bash

main() {
	local compiler=$(cat /proc/version | grep -oP 'gcc \w+ [\w.]+')
	local user=$(whoami)
	local kernel=$(uname -r)
	local host=$(uname -n)
	local date_time=$(uname -v)

	printf 'kernel version: %s\nuser: %s\nhost: %s\ncompiler: %s\ndate&time: %s\n' \
	"${kernel}" "${user}" "${host}" "${compiler}" "${date_time}"
}

main
```
