#include "../inc/converter.h"
#include <linux/device/class.h>

/* Static declaration begin */
static ssize_t value_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset);
static ssize_t value_read(struct file *file_p, char __user *buffer,size_t length,
		loff_t *offset);
static struct currency *get_currency(const char *token);
static size_t convert(size_t val, char *from, char *to);
static void to_value(char *buff);
static void convert_to_str(void);
static loff_t value_lseek(struct file *file, loff_t offset, int whence);
/* Static declaration end */

struct proc_ops proc_fops = {
    .proc_read  = value_read,
    .proc_write = value_write,
    .proc_lseek = value_lseek,
};

/* Static definition begin */
static ssize_t value_write(struct file *file_p, const char __user *buffer,
		size_t length, loff_t *offset)
{
	size_t left, len;
	len = (length >= BUFF_SIZE)? BUFF_SIZE-1: length;
	left = copy_from_user(&convert_buf[*offset], buffer, len); 
	if (left)
		pr_info("failed to read %lu chars from %lu", left, len);
	else
		pr_info("read %lu from %lu", len, length);
	convert_buf[len-left]='\0';
	*offset+=len-left;
	value_stamp=INVALID;
	return len-left;
}


static ssize_t value_read(struct file *file_p, char __user *buffer,size_t length,
		loff_t *offset)
{
	size_t len;
	if (value_stamp == INVALID) {
		to_value(convert_buf);
		value_stamp=VALID;
	}
	convert_to_str();
	len = strlen(convert_buf);
	if (*offset >= len) return 0;
	len = (length > len)? len: length;
	copy_to_user(buffer, &convert_buf[*offset], len);
	*offset += len;
	return len;
}

static loff_t value_lseek(struct file *file, loff_t offset, int whence)
{
	size_t buf_len;
	loff_t pos;
	buf_len = strlen(convert_buf);
	pos = file->f_pos;
	pr_info("lseek: %lld\n", offset);
	switch (whence) {
	case SEEK_SET: pos=offset; break;
	case SEEK_CUR: pos+=offset; break;
	case SEEK_END: pos=buf_len-offset; break;
	}
	if (pos > buf_len) pos=buf_len;
	if (pos < 0) pos=0;
	file->f_pos=pos;
	return pos;

}

static struct currency *get_currency(const char *token)
{
	struct currency *end, *cur; 
	if (strlen(token) < 2) goto wrong;
	end = &currencies[n_currencies-1];
	for (cur = currencies; cur <= end; ++cur) {
		if (token[0] == cur->token[0]
			&& token[1] == cur->token[1]) return cur;
	}

wrong:
	return NULL;
}


static size_t convert(size_t val, char *from, char *to)
{
	size_t res = val;
	struct currency *cur;
	if (!(cur=get_currency(from))) goto error;
	res *= cur->value;
	if (!(cur=get_currency(to))) goto error;
	return res/cur->value;
error:
	return -1;	
}


static void to_value(char *buff) 
{
	pr_info("%s --- ", buff);
	if (kstrtouint(buff, 10, &to_convert))
		to_convert=0;
}


static void convert_to_str(void)
{
	size_t res;
	pr_info("%u (val)", to_convert);
	res=convert(to_convert, convert_from, convert_to);
	if (res == (size_t) (-1)) {
	pr_info("ERROR: Converting %s %c to %c", convert_buf,
			*convert_from, *convert_to);
	}
	sprintf(convert_buf, "%lu\n", res);
}
/* Static definition end */



int init_converter_pfs(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}


void cleanup_converter_pfs(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}


static ssize_t from_show( struct class *class, struct class_attribute *attr,
		char *buf )
{
   sprintf(buf, "%s", convert_from);
   return strlen(buf);
}

static ssize_t to_show( struct class *class, struct class_attribute *attr,
		char *buf )
{
   sprintf(buf, "%s", convert_to);
   return strlen(buf);
}


static ssize_t to_store( struct class *class, struct class_attribute *attr,
		const char *buf, size_t count )
{
	struct currency *curr;
	if (!(curr=get_currency(buf))) goto wrong;
	pr_info("TO: Successfully written %.2s\n", buf);
	strcpy(convert_to, curr->token);
   	return strlen(buf);
wrong:
	sprintf(convert_to, "--");
	pr_err("TO: There is no %.2s currency token\n", buf);
	return strlen(buf);
}

static ssize_t from_store( struct class *class, struct class_attribute *attr,
		const char *buf, size_t count )
{
	struct currency *curr;
	if (!(curr=get_currency(buf))) goto wrong;
	pr_info("FROM: Successfully written %.2s\n", buf);
	strcpy(convert_from, curr->token);
   	return strlen(buf);
wrong:
	sprintf(convert_from, "--");
	pr_err("FROM: There is no %.2s currency token\n", buf);
	return strlen(buf);
}

CLASS_ATTR_RW(to);
CLASS_ATTR_RW(from);

struct class *converter_class;

int init_converter_sfs(void)
{
	int err;
	err = -1;
	converter_class = class_create(THIS_MODULE, "converter");
	if (IS_ERR(converter_class)) {
		pr_info("Failed to create converter class\n");
		goto end;
	}
	if ((err=class_create_file(converter_class, &class_attr_to))) {
		pr_info("Failed to create to-file\n");
		goto end;
	}
	if ((err = class_create_file(converter_class, &class_attr_from))) {
		pr_info("Failed to create from-file\n");
		goto end;
	}
end:
	return err;
}

void cleanup_converter_sfs(void)
{
	class_remove_file(converter_class, &class_attr_from);
	class_remove_file(converter_class, &class_attr_to);
	class_destroy(converter_class);
}
