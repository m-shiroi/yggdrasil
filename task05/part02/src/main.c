#include <stdlib.h>
#include <stdio.h>

#define MODULE_NAME "converter"
#define MODULE_FROM "/sys/class/"MODULE_NAME"/from"
#define MODULE_TO   "/sys/class/"MODULE_NAME"/to"
#define MODULE_VAL  "/proc/"MODULE_NAME"/value"
#define EUR 31
#define EUR_TAG "EU"
#define UAH 1
#define UAH_TAG "UA"

struct converter {
	FILE *from;
	FILE *to;
	FILE *val;
};

static int mod_is_loaded()
{
	return !system("lsmod|grep "MODULE_NAME" &>/dev/null");
}

static int converter_set(FILE *fp, const char *buf) 
{
	int res;
	res = fprintf(fp, "%s", buf);
	rewind(fp);
	return res;
}

static int converter_get(FILE *fp, char *buf) 
{
	int res;
	res = fscanf(fp, "%s", buf);
	rewind(fp);
	return res;
}

static inline int converter_get_to(struct converter *cnv, char *to)
{
	return converter_get(cnv->to, to);
}

static inline int converter_get_from(struct converter *cnv, char *from)
{
	return converter_get(cnv->from, from);
}

static inline int converter_get_val(struct converter *cnv, char *val)
{
	return converter_get(cnv->val, val);
}

static inline int converter_set_to(struct converter *cnv, const char *to)
{
	return converter_set(cnv->to, to);
}

static inline int converter_set_from(struct converter *cnv, const char *from)
{
	return converter_set(cnv->from, from);
}

static inline int converter_set_val(struct converter *cnv, const char *val)
{
	return converter_set(cnv->val, val);
}

int main(void)
{
	const char *value = "1000";
	struct converter converter;
	char content[32] = {'\0'};
	int ret = 0;
	if (!mod_is_loaded()) {
		fprintf(stderr, "Error: module %s is not loaded\n", MODULE_NAME);
		ret = -1;
		goto end;
	}
	converter.to = fopen(MODULE_TO, "r+");
	if (!converter.to) {
		fprintf(stderr, "Error: cannot open %s\n", MODULE_TO);
		ret = -1;
		goto end;
	}
	converter.from = fopen(MODULE_FROM, "r+");
	if (!converter.from) {
		fprintf(stderr, "Error: cannot open %s\n", MODULE_FROM);
		ret = -1;
		goto err_from;
	}
	converter.val = fopen(MODULE_VAL, "r+");
	if (!converter.val) {
		fprintf(stderr, "Error: cannot open %s\n", MODULE_VAL);
		ret = -1;
		goto err_val;
	}
	converter_set_from(&converter, EUR_TAG);
	converter_get_from(&converter, content);
	printf("%s %s\n", value, content);
	converter_set_to(&converter, UAH_TAG);
	converter_get_to(&converter, content);
	printf("to %s\n", content);
	converter_set_val(&converter, value);
	converter_get_val(&converter, content);
	printf("%s %s\n", content, UAH_TAG);

	fclose(converter.val);
err_val:
	fclose(converter.from);
err_from:
	fclose(converter.to);
end:
	return ret;
}
