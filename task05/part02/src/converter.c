#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include "../inc/converter.h"
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Maksym Bilyk");
MODULE_DESCRIPTION("Curency converter");
MODULE_VERSION("1.0");

struct currency currencies[] = {{.token="UA",.value=1*STORY_POINT}, {.token="EU",.value=32*STORY_POINT}};
size_t n_currencies = sizeof(currencies)/sizeof(struct currency);
struct proc_dir_entry *proc_dir;
struct proc_dir_entry *proc_file;
char convert_buf[BUFF_SIZE];

unsigned int to_convert = 0;
unsigned int converted = 0;
char convert_from[TAG_LENGTH] = "UA";
char convert_to[TAG_LENGTH] = "EU";
enum time_stamp value_stamp = INVALID;


static int __init converter_init(void)
{
	ssize_t res;
	if ((res=init_converter_pfs())) goto err;
	if ((res=init_converter_sfs())) goto err;
	pr_info("Module successfully loaded\n");
	return 0;
err:
	pr_info("Error");
	return res;
}

static void __exit converter_exit(void)
{
	cleanup_converter_pfs();
	cleanup_converter_sfs();
}

module_init(converter_init);
module_exit(converter_exit)

