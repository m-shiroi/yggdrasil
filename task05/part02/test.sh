#! /bin/bash

declare -i UA=1
declare -i EU=32
declare -i N_PASSED=0
declare -i N_FAILED=0

build_module() { make &>/dev/null;insmod converter.ko; }
clean_module() { make clean &>/dev/null;rmmod converter; }

module_convert() {
	echo ${1} > /sys/class/converter/from
	echo ${2} > /sys/class/converter/to
	echo ${3} > /proc/converter/value
	cat /proc/converter/value
}

convert() {
	local from=${1};local to=${!2};local value=${3}
	echo $(((value*from)/to))
}

assert_eq() {
	if [ ${1} -eq ${2} ]; then
		echo "passed"
		(( ++N_PASSED ))
	else
		echo "failed: expected ${1} got ${2}"
		(( ++N_FAILED ))
	fi
}

run_test() {
	echo "Converting ${3} ${1} to ${2}"
	local expected=$(convert "${1}" "${2}" ${3})
	local real=$(module_convert "${1}" "${2}" ${3})	
	assert_eq ${expected} ${real}
	echo "---"
}

main() {
	build_module
	local values=(1 10 100 1000 10000 100000 1000000 10000000)
	for val in ${values[@]}; do
		run_test "EU" "UA" ${val}
		run_test "UA" "EU" ${val}
	done
	echo -e "Summary:\n\tPassed: ${N_PASSED}\n\tFailed: ${N_FAILED}"
	clean_module
}
main 
