#include "../inc/converter.h"

struct currency *get_currency(char token)
{
	struct currency *end, *cur; 
	end = &currencies[n_currencies-1];
	for (cur = currencies; cur <= end; ++cur) {
		if (token == cur->token) return cur;
	}
	return NULL;
}

size_t convert(size_t val, char from, char to)
{
	size_t res = val;
	struct currency *cur;
	if (!(cur=get_currency(from))) goto error;
	res *= cur->value;
	if (!(cur=get_currency(to))) goto error;
	return res/cur->value;
error:
	return -1;	
}
