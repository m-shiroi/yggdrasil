#ifndef __MY_CONVERTER_H
#define __MY_CONVERTER_H
#include <linux/kernel.h>

#define STORY_POINT 1

struct currency{
	char token;
	size_t value;
};



extern struct currency currencies[];
extern size_t n_currencies;

extern unsigned int converter_value;
extern char *convert_from;
extern char *convert_to;

struct currency *get_currency(char token);
size_t convert(size_t val, char from, char to);

#endif // __MY_CONVERTER_H
