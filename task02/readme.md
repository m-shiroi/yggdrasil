Task: write a ***Guess the Number*** game using the Bash script.
Subtask: create a random number generator script. 

All commits in branch rngen:
- 74993f2 task02: add a random number generator bash-script

Subtask completed

Subtask: Extract Function refactoring.

All commits in branch rngen_ref:
- e0b062a task02: add random range support to bash-script

Subtask completed

Subtask: Add user interaction

All commits in branch_ui:
- 03125fc task02: add user guess validation

Subtask completed

Subtask: add user-guess prompt
- c8d7112 subtask5: add user-guess prompt

Subtask completed

Subtask: add default range

Subtask completed

Subtask: complex (add better user interaction)
