#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	struct node *next;
	unsigned int n;
	char sym;
} RLE_node;

void init_node(RLE_node *node, unsigned int n, char sym) {
	node->n=n;
	node->sym=sym;
	node->next=NULL;
}

RLE_node* create_node(void) {
	return malloc(sizeof(RLE_node));
}

RLE_node* add_node(RLE_node *tail, unsigned int n, char sym) {
	if (!tail->n) {
		init_node(tail, n, sym);
		return tail;
	}
	
	RLE_node *new_node=create_node();
	init_node(new_node, n, sym);
	tail->next=new_node;
	return new_node;
}

void unpack_node(RLE_node *node) {
	for (unsigned int counter=node->n; counter > 0; --counter) {
		printf("%c", node->sym);
	}
}

void pack_node(RLE_node *node) {
	printf("%c%u", node->sym, node->n);
}

void iter_nodes(RLE_node *head, void (*func)(RLE_node*)) {
	for (; head; head=head->next) {
		func(head);
	}
}



void delete_nodes(RLE_node *head) {
	RLE_node* prev=head;
	RLE_node* cur;
	for (cur = prev->next; cur; cur=cur->next) {
		free(prev);
		prev=cur;
	}
	if (prev) 
		free(prev);
}

void fill_unpacked(RLE_node *tail) {
	int c_cur=getchar();
	int c_prv=c_cur;
	unsigned int counter=1;
	while ((c_cur=getchar()) != EOF) {
		if (c_cur != c_prv) {
			tail=add_node(tail, counter, c_prv);
			counter=1;
		}
		else {
			++counter;
		}
		c_prv=c_cur;
	}
}

void fill_packed(RLE_node *tail) {
	int chr=getchar();
	int num=getchar();
	while (chr != EOF && num != EOF)
	{
		tail=add_node(tail, atoi((char*)&num), chr);
		chr=getchar();
		num=getchar();
	}
}



int main(void) {
	RLE_node *head, *tail;
	head = create_node();
	
#ifdef DECODE
	fill_packed(head);	
	iter_nodes(head, unpack_node);
#endif

#ifndef DECODE
	fill_unpacked(head);
	iter_nodes(head, pack_node);
#endif
	delete_nodes(head);
}
