# Exercise 10. Virtual file system. Programming interface. Resources

### Description

Implement a kernel module for translating text messages.

### References

- [procfs](https://gitlab.com/gl-khpi/examples/tree/master/procfs_rw)
- [sysfs](https://gitlab.com/gl-khpi/examples/tree/master/sysfs)

| Use Git and the following names: exercise10 - for your project's home directory;
| partXX - for the directory of each subtask (XX - subtask number); 
| src - for the source code directory

### Guidance

1. Subtask I. Build and test [Procfs demo module](https://gitlab.com/gl-khpi/examples/tree/master/procfs_rw).
2. Subtask II. Develop a kernel module:
	- to flip all words in a string;
	- to make a string uppercase.
3. Stubask III. Add the interfaces into the module:
	- procfs to store/display user string.
	- sysfs to select the method of translation:
		- 0 - no translation;
		- 1 - flip;
		- 2 - uppercase.

### Extra
	- Implement bash script for the module testing
	- Provide a test scenario
