#include <linux/string.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Maksym Bilyk");
MODULE_DESCRIPTION("Example for procfs read/write");
MODULE_VERSION("0.1");


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "example"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     1000

enum order {
	STRAIGHT,
	REVERSED,
	CAPITALIZED
};

static char *help_buffer;
static char *proc_buffer;
static size_t proc_msg_length;
static size_t help_msg_length;

static enum order read_mod = REVERSED;
static enum order prev_read_mod = STRAIGHT;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static struct proc_ops proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};

static char capitalize(char chr)
{
	return (chr >= 'a' && chr <= 'z')? chr - ('a'-'A'): chr;
}

static void swap_word(char *begin, char *end)
{
	char tmp;
	if (*end=='\n') --end;
	while (begin <= end) {
		tmp = *end;
		*end = *begin;
		*begin = tmp;
		++begin;
		--end;
	}
}

static void swap_words(char *first, char *last)
{
	char *begin = first-1;
	char *end = memchr(first, ' ', first-last+1);
	while (end) {
		swap_word(begin+1, end-1);
		begin=end;
		end=memchr(begin + 1, ' ', last-begin);
	}
	if (begin < last)
		swap_word(begin+1, last);
}

static void cap_words(char *first, char *last)
{
	char *cur;
	for (cur=first; cur <= last; ++cur) {
		*cur=capitalize(*cur);
	}
}

static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    help_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == help_buffer)
	    goto err;
    proc_msg_length = 0;
    help_msg_length = 0;
    return 0;
err:
    kfree(proc_buffer);
    return -ENOMEM;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    if (help_buffer) {
        kfree(help_buffer);
        help_buffer = NULL;
    }
    help_msg_length = 0;
    proc_msg_length = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}



static ssize_t opts_show( struct class *class, struct class_attribute *attr, char *buf ) {
   sprintf(buf, "%d", read_mod);
   printk( "read %ld\n", (long)strlen( buf ) );
   return strlen( buf );
}

static ssize_t opts_store( struct class *class, struct class_attribute *attr, const char *buf, size_t count ) {
   printk( "write %ld\n", (long)count );
   switch (buf[0]) {
   case '0': read_mod=STRAIGHT; break;
   case '1': read_mod=REVERSED; break;
   case '2': read_mod=CAPITALIZED; break;
   default: break;
   }
   return count;
}

static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;
    char *buf;	

    switch (read_mod) {
    case STRAIGHT: buf=proc_buffer; break;
    case REVERSED: 
	buf=help_buffer;
	if (prev_read_mod == read_mod) break;
    	memcpy(buf, proc_buffer, proc_msg_length);
	swap_words(buf, &buf[proc_msg_length-1]);
	break;
    case CAPITALIZED:
	buf=help_buffer;
	if (prev_read_mod == read_mod) break;
	memcpy(buf, proc_buffer, proc_msg_length);
	cap_words(buf, &buf[proc_msg_length-1]);
	break;
    default: break;
    };


    if (length > (proc_msg_length - *offset))
        length = (proc_msg_length - *offset);

    left = copy_to_user(buffer, &buf[*offset], length);
    *offset+=length;


    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %lu from %lu chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %lu chars\n", length);

    return length - left;
}


static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %lu to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = copy_from_user(&proc_buffer[*offset], buffer, msg_length);
	
    if (*offset)
    	proc_msg_length += msg_length - left;
    else
	proc_msg_length = msg_length - left;
    *offset+=msg_length;
    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %lu from %lu chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", msg_length);

    return length;
}


CLASS_ATTR_RW( opts ); //, ( S_IWUSR | S_IRUGO ), &x_show, &x_store );
static struct class *opts_class;

static int __init example_init(void)
{
    int err;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

    opts_class = class_create( THIS_MODULE, "string_opts" );
    if ( IS_ERR(opts_class) ) printk("bad class create\n");
    err = class_create_file(opts_class, &class_attr_opts);
    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    return err;
}


static void __exit example_exit(void)
{
    cleanup_proc_example();
    cleanup_buffer();
    class_remove_file(opts_class, &class_attr_opts);
    class_destroy(opts_class);
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}


module_init(example_init);
module_exit(example_exit);

