#! /bin/env bash

declare -ir id=0x68
declare -ir line=1
declare -ir MODE_FLAG=6
declare -ir AP_FLAG=5
declare -ir pinN=26
declare -ir limit=10
declare -r dItv=0.01
declare -i isDone=0

trap "turn_off_gpioN ${pinN}; exit" 2    

get_Nth_bit() {
	local b=${1}
	local n=${2}
	local pos=$(( (n/4+1)%2 ))

	b=${b:pos:1}
	((n=n%4))
	echo $(( (b&(2**n))>>n ))
}

is_12() {
	echo $( get_Nth_bit ${1} ${MODE_FLAG} )
}

is_PM() {
	echo $( get_Nth_bit ${1} ${AP_FLAG} )
}

read_reg() {
	i2cget -y ${line} ${id} ${1}
}

get_time() {
	local pref
	local reg
	local t
	case ${1} in
		s) reg=0x00;;
		m) reg=0x01;;
		h) reg=0x02;;
	esac

	t=$(read_reg ${reg})
	t=${t:2:2}

	

	if [[ ${1} != 'h' || $(is_12 ${t}) -ne 1 ]]; then
		echo ${t}
		return
	fi

	if (( $(is_PM ${t}) )); then pref="PM "; else pref="AM "; fi
	(( t=(${t:0:1}&1)*10 + ${t:1:1} ))
	echo "${pref}${t}"

}


turn_on_gpioN_in() {
        echo ${1} > /sys/class/gpio/export             
        echo in   > /sys/class/gpio/gpio${1}/direction 
}

turn_off_gpioN() {
        echo ${1} > /sys/class/gpio/unexport           
}

read_gpioN() {
        cat /sys/class/gpio/gpio${1}/value             
}

get_click() {
	local counter=0
        local cur=$(read_gpioN ${1})                   
        while (( cur != 1 )); do
                sleep ${dItv}
                cur=$(read_gpioN ${1})                 
        done
        while (( cur != 0 )); do
                sleep ${dItv}
                cur=$(read_gpioN ${1})                 
		(( counter++ ))
        done
	
	if (( counter > limit )); then
		isDone=1;
	fi
	echo "$(get_time h):$(get_time m):$(get_time s)"
}




main() {
	turn_on_gpioN_in ${pinN}
	while (( !isDone )); do
		get_click ${pinN}
	done
	turn_off_gpioN ${pinN}
}

main $@
