#include <linux/init.h> 
#include <linux/kernel.h> 
#include <linux/module.h> 
#include <linux/list.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/device/class.h>

#define MODULE_TAG     "task_08"

enum reminder_state {
	ACTIVE,
	SHOWN
};

enum show_mod {
	ALL,
	SINGLE
};

struct reminder_entry {
	struct list_head r_entry;
	size_t id;
	u32 time_point;
	enum reminder_state state;
	char msg[];
};

static struct list_head reminder_list;
static size_t user_id = -1;

static u32 get_time_s(void)
{
	u32 cur=jiffies;
	return cur/HZ;
}

static void reminder_add_entry(char *msg, size_t id, u32 interval)
{
	struct reminder_entry *new;
	new = kmalloc(sizeof(struct reminder_entry)+strlen(msg)+1, GFP_KERNEL);
	strcpy(new->msg, msg);
	new->id=id;
	new->time_point=get_time_s() + interval;
	new->state=ACTIVE;

	INIT_LIST_HEAD(&new->r_entry);
	list_add_tail(&new->r_entry, &reminder_list);
}

static void free_reminders(void)
{
	struct reminder_entry *temp, *cursor;
	list_for_each_entry_safe(cursor, temp, &reminder_list, r_entry) {
		pr_info("Deleting reminder with string: %s\n", cursor->msg);
		list_del(&cursor->r_entry);
        	kfree(cursor);
        }
}

static void show_user_reminders(size_t id, enum show_mod mod, char *buf) {
	struct reminder_entry *temp;
	s32 time_left;
	u32 cur_time;
	cur_time = get_time_s();

	list_for_each_entry(temp, &reminder_list, r_entry) {
		if (mod == ALL && temp->state == ACTIVE) {
			time_left=temp->time_point - cur_time;
			if (buf != NULL)
				sprintf(buf, "%zu:%d: %s\n", temp->id, time_left, temp->msg);
			else
				pr_info("%zu:%d: %s\n", temp->id, time_left, temp->msg);
			if (time_left <= 0) temp->state=SHOWN;
		} else if (temp->id == id && temp->state == ACTIVE) {
			time_left=temp->time_point - cur_time;
			if (buf != NULL)
				sprintf(buf, "%zu:%d: %s\n", id, time_left, temp->msg);
			else
				pr_info("%zu:%u: %s\n", id, temp->time_point, temp->msg);
			if (time_left <= 0) temp->state=SHOWN;
		}
	}
}

static ssize_t message_show(struct class *class, struct class_attribute *attr,
		char *buf)
{
	if (user_id == -1)
		show_user_reminders(0, ALL, buf);
	else
		show_user_reminders(user_id, SINGLE, buf);
	return strlen(buf);
}

static ssize_t message_store( struct class *class, struct class_attribute *attr,
		const char *buf, size_t count )
{
	char message[100];
	u32 time_interval;
	if (user_id != -1 && sscanf(buf, "%u %s", &time_interval, message) != 2) {
		pr_info("Error: reading %s\n", buf);
		goto end;
	}
	reminder_add_entry(message, user_id, time_interval); 

end:
	return strlen(buf);
}

static ssize_t user_id_show(struct class *class, struct class_attribute *attr,
		char *buf)
{ 
	if (user_id == -1)
		sprintf(buf, "user_id: NOT SET YET\n");
	else
		sprintf(buf, "user_id: %zu\n", user_id);
	return strlen(buf);
}

static ssize_t user_id_store( struct class *class, struct class_attribute *attr,
		const char *buf, size_t count )
{ 
	sscanf(buf, "%zu", &user_id);
	return strlen(buf);

}

CLASS_ATTR_RW( message );
CLASS_ATTR_RW( user_id );

static struct class *reminder_class;

static int __init reminder_init(void)
{
	char *str_1 = "<> str 1";
	char *str_2 = "<> str 2";
	char *str_3 = "<> str 3";
	int err;

	INIT_LIST_HEAD(&reminder_list);
	
	reminder_class = class_create(THIS_MODULE, MODULE_TAG);
	if (IS_ERR(reminder_class)) printk("bad class create\n");
	err = class_create_file(reminder_class, &class_attr_message);
	err = class_create_file(reminder_class, &class_attr_user_id);
	pr_info("Init reminder\n");
	return 0;
}

static void __exit reminder_exit(void)
{
	//show_user_reminders(1, ALL, NULL);
	free_reminders();

	class_remove_file(reminder_class, &class_attr_message);
	class_remove_file(reminder_class, &class_attr_user_id);
    	class_destroy(reminder_class);
	pr_info("Exit reminder\n");
}

module_init(reminder_init);
module_exit(reminder_exit);
MODULE_LICENSE("GPL");
