#! /bin/env bash

declare -ir ERROR=1
declare -ir SUCCESS=0
declare -ir FLAG_IS_SET=1

declare -i H_FLAG=0
declare -i T_FLAG=0
declare -i R_FLAG=0
declare -i Y_FLAG=0

declare -a user_dirs

parse_args() {
	for arg; do
		case "${arg}" in
			-h|--help) ((H_FLAG=FLAG_IS_SET));;
			-t|--test) ((T_FLAG=FLAG_IS_SET));;
			-r|--recursive) ((R_FLAG=FLAG_IS_SET));;
			-y|--yes) ((Y_FLAG=FLAG_IS_SET));;
			*) user_dirs+=("${arg}")
		esac
	done
}

help_info() {
	if (( H_FLAG == FLAG_IS_SET )); then
		echo "  --help"
		echo "  --test"
		echo "  --recursive"
		echo "  --yes"
		exit ${SUCCESS}
	fi
}

test_rm() {
	for fl in $@; do
		if [[ -f "$fl" ]]; then
			echo "remove ${CONFIRM}: $fl"
		fi
	done
}

real_rm() {
	if (( Y_FLAG == FLAG_IS_SET)); then
		yes | rm -f $@
	else
		rm -f $@
	fi
}

recursive_rm () {
	local cur_dir=$1
	local cur_fls=(${cur_dir}/{*.tmp,~*,-*,_*})
	local nested_dirs=$(echo ${cur_dir}/*/)

	if (( T_FLAG == FLAG_IS_SET )); then
		test_rm ${cur_fls[@]}
	else
		real_rm ${cur_fls[@]}
	fi

	if (( R_FLAG != FLAG_IS_SET )); then
		return ${SUCCESS}
	fi

	for dr in ${nested_dirs[@]}; do
		test -d ${dr} && recursive_rm ${dr%/}
	done
	
	if (( $(ls ${cur_dir} -a | wc -w) == 2 )); then
		rm -r ${cur_dir}
	fi

	return ${SUCCESS}
}

main() {
	parse_args $@
	help_info

	declare -r CONFIRM="$( (( Y_FLAG == FLAG_IS_SET )) && echo "(yes)" || echo "" )"
	user_dirs=${user_dirs:='.'}
	for u_dir in "${user_dirs[@]}"; do
		if [[ -d $u_dir ]]; then
			recursive_rm $u_dir
		fi
	done
	exit ${SUCCESS}
}

main $@