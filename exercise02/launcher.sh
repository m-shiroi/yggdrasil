#! /bin/env bash

declare -ri CONTINUE=3

main () {
    ./script.sh $1
    if (($? == CONTINUE)); then
        ../exercise01/script.sh $1
    fi
}

main $@