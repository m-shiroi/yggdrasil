#! /bin/env bash

declare -ri SUCCESS=1
declare -ri FAIL=2
declare -ri CONTINUE=3


err () {
	>&2 echo $@
}


check_dir () {
	local my_dir=$1
	if [[ ! -d $my_dir ]]; then
		err "Error: no such dir: ${my_dir}"
		exit ${FAIL}
	fi
}


main () {
	local legacy_counter=0
	local script="$(readlink -f $0)"
	local launcher="$(dirname $(readlink -f $0))/launcher.sh"
	local path=$1
	path=${path:-"."}
	check_dir $path

	for cur in $(find $path -maxdepth 1 -mtime +30); do 
		if [[ -f $cur && ! $cur = "$script" \
					  && ! $cur = "$launcher" ]]; then
			((legacy_counter+=1))
			local mod="$(dirname $cur)/~$(basename $cur)"
			mv $cur $mod 
			touch $mod
		fi
	done
	
	if ((legacy_counter > 0)); then
		exit ${CONTINUE}
	fi
}

main $@