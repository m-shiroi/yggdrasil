#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "list.h"


enum status {
	SUCCESS,
	USER_LIMIT,
	UNDEF_USER
};

#define BUFF_LEN 50
#define EDGE_NUN 6
#define USER_MAX 10
#define SCORE_MAX 100
#define UNLUCKY_NUM 1 


struct player {
	char name[BUFF_LEN];
	unsigned score;
	struct list_head llist;
};

unsigned throw_dice(void)
{
	return rand()%(EDGE_NUN)+1;
}

const char *get_format(void)
{
	static char format[10];
	if (!*format) {	
		sprintf(format, "%%%ds", BUFF_LEN-1);
	}
	return format;
}

int play(struct player *p)
{
	unsigned cur_score=0;
	unsigned dice;
	int answ;
	printf("---\n%s's turn\n", p->name);
	while (1) {	
		printf("0 - throw dice\n* - pass\n%s> ", p->name);
		if (scanf("%d", &answ) != 1) {
			scanf("%*s");
			break;
		}
		if (answ)
			break;
		dice=throw_dice();
		if (dice == UNLUCKY_NUM) 
			cur_score = 0;
		else
			cur_score += dice;
		printf("DICE: %u\nTurn total so far: %u\n", dice, cur_score);
		if (!cur_score || cur_score+p->score >= SCORE_MAX)
			break;
	}
	p->score += cur_score;
	printf("%s's total score: %u\n", p->name, p->score);
	return (p->score >= SCORE_MAX)? 0: 1;
}

static int init_user(struct list_head *head)
{
	struct player *p;
	p=(struct player*)malloc(sizeof(struct player));
	INIT_LIST_HEAD(&p->llist);
	list_add_tail(&p->llist, head);	

	p->score=0;
	puts("Enter user name:");
	return (scanf(get_format(), p->name)==EOF)? UNDEF_USER: SUCCESS;
}


enum status init_users(struct list_head *head, unsigned unum)
{
	enum status err = SUCCESS;
	for (unsigned i=0; i<unum; ++i) {
		if ((err=init_user(head))) {
			fputs("Error, empty user name\n", stderr);
			break;
		}
	}
	return err;
}

void show_results(struct list_head *head)
{
	struct player *cur, *next;
	const char *is_winner;
	puts("Game results:");
	list_for_each_entry_safe(cur, next, head, llist) {
		is_winner = (cur->score < SCORE_MAX)? "": "(winner)";
		printf("%s: %u %s\n", cur->name, cur->score, is_winner);
		free(cur);
	}
}

enum status config(struct list_head *head)
{
	unsigned unum = 0;
	enum status err = SUCCESS;
	puts("Wellcome to Pig (dice game)\nEnter the number of players:");
	scanf("%u", &unum);
	if (!unum || unum > USER_MAX) {
		fprintf(stderr, "Error, user limit exceeded.\n"
				"Expected: up to %d, got %u\n", USER_MAX, unum);
		err=USER_LIMIT;
		goto end;
	}
	err = init_users(head, unum);

end:
	return err;
}

int main(void)
{
	struct player *cur_player;
	LIST_HEAD(game);
	config(&game);
	for (;;) {
		list_for_each_entry(cur_player, &game, llist) {
			if (!play(cur_player))
				goto end;
		}
	}
end:
	show_results(&game);
	return 0;
}
